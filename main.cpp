// Program: key-decryptor
// Last updated: October 7, 2017
// Compile: i686-w64-mingw32-g++ -wall -std=c++11 -static -o Decyrptor.exe main.cpp

#include <iostream>
#include <fstream>
#include <vector>

std::string base64Decode(const std::string &s);

std::string decryptBase64 (std::string s);

int main(int argc, char *argv[]) // needs input file and output file
{
  if (argc != 3) // two arguments, plus program itself
    {
      std::cerr << "\nProgram requires two arguments, input and output.\n\n";
      return 1;
    }

  std::string input = argv[1]; // argument 1 is the input file
  std::string output = argv[2]; // argument 2 is the output file
  std::ifstream inFile;

  inFile.open(input);
  if (inFile.fail())
    {
      std::cerr << "\nCannot read or open input file '" << input << "'\n\n";
      inFile.close();

      return 2;
    }

  std::string data;

  inFile >> data;
  if (inFile.fail())
    {
      std::cerr << "\nInput file '" << input << "' is corrupt!\n\n";
      inFile.close();

      return 3;
    }

  inFile.close();
  data = decryptBase64(data);

  std::ofstream outFile;

  outFile.open(output);
  if (outFile.fail())
    {
      std::cerr << "\nCannot create output file '" << output << "'\n\n";
      outFile.close();

      return 4;
    }

  outFile << data;
  if (outFile.fail())
    {
      std::cerr << "\nCannot write to output file '" << output << "'\n\n";
      outFile.close();

      return 5;
    }

  outFile.close();

  std::cout << "\nDecrypting succesful!\n\n";

  return 0;
}

std::string base64Decode(const std::string &str)
{
  const std::string &BASE64_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // pass string itself with &
  const unsigned int BASE_256 = 0xFF;

  std::vector<int> ASCII_position(256, -1); // 256 ints with value of -1

  for (int i = 0; i < 64; i++)
    ASCII_position[BASE64_TABLE[i]] = i; // initialize vector with ASCII character index, non-Base64 character indexes have a value of -1

  std::string decoded;
  int value = 0;
  int bits = -8;

  for (const char &character : str)
    {
      if (ASCII_position[character] == -1) // if non-Base64 character, e.g. '=' paddings
        break;

      value = (value << 6) + ASCII_position[character]; // add binary representation of Base64 character to value
      bits += 6; // 6 bits or 1 Base64 character processed

      if (bits >= 0) // if at least 8 bits have been added to value then process the byte
        {
          // if bits 4 (first 2 Base64 characters) value is right-shifted by 4 (from being left-shifted by 6 twice, selecting 8 bits)
          // this is checked against BASE_256 (8 bits) and appended to decoded,
          // 4 bits are left (bits = -4), + 6 = 2 bits (10 bits total)

          // if bits 2 (1 more Base64 character after first 2) value is right-shifted by 2 (from 10 to get 8)
          // this is checked against BASE_256 (8 bits) and appeneded to decoded,
          // 2 bits are left (bits = -6), + 6 = 0 bits (8 bits total)

          // if bits 0 (1 more, to make 4 total, Base64 character after if 2 bits) value is not right-shifted, (selecting all the last byte)
          // this is checked against BASE_256 (8 bits) and appended to decoded,
          // no bits are left, recycle process

          decoded.push_back(char((value >> bits) & BASE_256)); // append byte-sized character to decoded
          bits -= 8; // 1 byte processed
        }
    }

  return decoded;
}

std::string decryptBase64 (std::string str)
{
  // Salts
  const std::string SALT[13] = {"c3G/&n", ",Fs@hLO", "Abv", "T*xqS", "tImU*M4P", "|&v7|", "k",
                                "%CnXRix", "?jS", "Vak$6", "MOf@u=v=", "&w%w4'_L3|,w:EzS", "3u=:"};

  str = base64Decode(str);
  str = str.substr(0, (str.length() - SALT[4].length()));
  str = str.substr(0, (str.length() / 3));

  str = str.erase(12, SALT[2].length());
  str = str.erase(15, SALT[6].length());
  str = str.erase(6, SALT[0].length());
  str = str.erase(8, SALT[3].length());
  str = str.erase(2, SALT[3].length());
  str = str.erase(3, SALT[4].length());

  str = str.substr(0, (str.length() - SALT[5].length()));
  str = str.substr((SALT[8].length() + SALT[1].length()), (str.length() - (SALT[8].length() + SALT[1].length())));

  str = str.erase(7, 1);
  str = str.erase(1, 1);

  str = base64Decode(str);
  str = str.substr(0, (str.length() - (SALT[9].length() + SALT[12].length() + SALT[11].length())));
  str = base64Decode(str);

  str = str.substr(0, (str.length() - SALT[10].length()));
  str = str.erase(7, SALT[2].length());
  str = base64Decode(str);

  str = str.substr(0, (str.length() - (SALT[11].length() + SALT[12].length())));
  str = str.substr(SALT[10].length(), (str.length() - SALT[10].length()));

  return str;
}
